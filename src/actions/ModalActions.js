import dispatcher from '../dispatcher';

export function openModal(title, content, callback) {
	dispatcher.dispatch({
		type: 'OPEN_MODAL',
		title,
		content,
		callback
	});
}

export function closeModal() {
	dispatcher.dispatch({
		type: 'CLOSE_MODAL'
	});
}