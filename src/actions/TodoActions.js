import dispatcher from '../dispatcher';

export function createTodo(name, priority) {
	dispatcher.dispatch({
		type: 'ADD_TODO',
		name,
		priority
	});
}

export function toggleTodo(index, status) {
	dispatcher.dispatch({
		type: 'TOGGLE_TODO',
		index,
		status
	});
}

export function toggleAllTodos(status) {
	dispatcher.dispatch({
		type: 'TOGGLE_ALL_TODOS',
		status
	});
}

export function removeTodo(index) {
	dispatcher.dispatch({
		type: 'REMOVE_TODO',
		index
	});
}

export function removeAllTodos() {
	dispatcher.dispatch({
		type: 'REMOVE_ALL_TODOS'
	});
}

export function removeAllDoneTodos() {
	dispatcher.dispatch({
		type: 'REMOVE_ALL_DONE_TODOS'
	});
}