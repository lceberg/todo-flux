import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';

class TodoStore extends EventEmitter {
	constructor() {
		super();

		this.todos = {
			items: []
		};

		this.todos.completed = this.getDoneTodos();
	}

	getTodos() {
		return this.todos;
	}

	getDoneTodos() {
		let completed = 0;
		let todos = this.todos.items.map((todo) => {
			if ( todo.done ) {
				completed++;
			}
		});

		return completed;
	}

	handleActions(action) {
		switch(action.type) {
			case 'ADD_TODO': {
				this.createTodo(action.name, action.priority);
				break;
			}

			case 'TOGGLE_TODO': {
				this.toggleTodo(action.index, action.status);
				break;
			}

			case 'TOGGLE_ALL_TODOS': {
				this.toggleAllTodos(action.status);
				break;
			}

			case 'REMOVE_TODO': {
				this.removeTodo(action.index);
				break;
			}

			case 'REMOVE_ALL_TODOS': {
				this.removeAllTodos();
				break;
			}

			case 'REMOVE_ALL_DONE_TODOS': {
				this.removeAllDoneTodos();
				break;
			}
		}
	}

	createTodo(name, priority) {
		if ( ! name ) {
			return;
		}

		const id = new Date().getTime();

		this.todos.items.push({
			id,
			name,
			priority,
			done: false
		});

		this.emit('change');
	}

	toggleTodo(index, status) {
		let completed = 0;
		let todos     = this.todos.items;

		todos.map((todo, key) => {
			if ( todo.id === index ) {
				todo.done = status;
			}

			if ( todo.done ) {
				completed++;
			}
		});

		this.todos.items     = todos;
		this.todos.completed = completed;

		this.emit('change');
	}

	toggleAllTodos(status) {
		let todos = this.todos.items;

		todos.map((todo, index) => {
			todo.done = status;
		});

		this.todos.items     = todos;
		this.todos.completed = status ? todos.length : 0;

		this.emit('change');
	}

	removeTodo(index) {
		let completed = 0;
		let todos     = this.todos.items;

		todos.map((todo, key) => {
			if ( todo.id === index ) {
				todos.splice(key, 1);
				return;
			}

			if ( todo.done ) {
				completed++;
			}
		});

		this.todos.items     = todos;
		this.todos.completed = completed;

		this.emit('change');
	}

	removeAllTodos() {
		this.todos.completed = 0;
		this.todos.items = [];

		this.emit('change');
	}

	removeAllDoneTodos() {
		for (let i = this.todos.items.length -1; i >= 0; i--) {
			if ( ! this.todos.items[i].done ) {
				continue;
			}

			this.todos.items.splice(i, 1);
		}

		this.todos.completed = 0;

		this.emit('change');
	}
}

const todoStore = new TodoStore;

dispatcher.register(todoStore.handleActions.bind(todoStore));

export default todoStore;