import { EventEmitter } from 'events';
import dispatcher from '../dispatcher';

class ModalStore extends EventEmitter {
	constructor() {
		super();

		this.modal = {
			active: false,
			title: '',
			content: '',
			callback: false
		};
	}

	getModalData() {
		return this.modal;
	}

	handleActions(action) {
		switch(action.type) {
			case 'OPEN_MODAL': {
				this.openModal(action.title, action.content, action.callback);
				break;
			}

			case 'CLOSE_MODAL': {
				this.closeModal();
				break;
			}
		}
	}

	openModal(title, content, callback) {
		this.modal = {
			active: true,
			title,
			content,
			callback
		};

		this.emit('change');
	}

	closeModal() {
		this.modal = {
			active: false,
			title: '',
			content: '',
			callback: false
		};

		this.emit('change');
	}
}

const modalStore = new ModalStore;

dispatcher.register(modalStore.handleActions.bind(modalStore));

export default modalStore;