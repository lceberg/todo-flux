import React from 'react';

class TodoFilter extends React.Component {
	render() {
		return (
			<div>
				<div className="control">
					<input type="text" className="input" placeholder="Filter By Name" value={this.props.name} onChange={this.props.handleName} />
				</div>

				<div className="control">
					<div className="select is-fullwidth">
						<select value={this.props.priority} onChange={this.props.handlePriority}>
							<option value="">-- Filter By Priority --</option>
							<option value="Low">Low</option>
							<option value="Medium">Medium</option>
							<option value="High">High</option>
						</select>
					</div>
				</div>
			</div>
		);
	}
}

export default TodoFilter;