import React from 'react';
import * as TodoActions from 'actions/TodoActions';

class TodoInput extends React.Component {
	constructor() {
		super();

		this.state = {
			todo    : '',
			priority: 'Low',
			disabled: true
		}

		this.handleInput = this.handleInput.bind(this);
		this.submit      = this.submit.bind(this);
	}

	componentDidMount() {
		this.todo.focus();
	}

	render() {
		return (
			<form method="post" onSubmit={this.submit}>
				<p className="control has-addons">
					<input className="input is-expanded" type="text" value={this.state.todo} placeholder="What needs to be done ?" ref={(input) => {this.todo = input}} onChange={this.handleInput} />
					<span className="select">
						<select value={this.state.priority} onChange={this.handleInput} ref={(input) => {this.priority = input}}>
							<option value="">-- Priority --</option>
							<option value="Low">Low</option>
							<option value="Medium">Medium</option>
							<option value="High">High</option>
						</select>
					</span>
					<button type="submit" className="button is-primary" disabled={this.state.disabled}>Add Todo</button>
				</p>
			</form>
		);
	}

	handleInput(event) {
		this.setState({
			todo    : this.todo.value,
			priority: this.priority.value,
			disabled: ! this.todo.value.trim() || ! this.priority.value.trim()
		});
	}

	submit(event) {
		event.preventDefault();
		TodoActions.createTodo(this.todo.value, this.priority.value);
		this.setState({
			todo    : '',
			priority: 'Low',
			disabled: true
		});

		this.todo.focus();
	}
}

export default TodoInput;