import React from 'react';

class TodoProgress extends React.Component {
	render() {
		let total      = this.props.count;
		let completed  = this.props.completed;
		let percentage = 0;

		if ( completed ) {
			percentage = Math.ceil( Math.min( ( ( 100 / total ) * completed ), 100 ) );
		}

		return (
			<progress className="progress is-primary" value={percentage} max="100">{percentage}%</progress>
		);
	}
}

export default TodoProgress;