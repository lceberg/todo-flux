import React from 'react';
import TodoProgress from 'components/TodoProgress';
import TodoFilter from 'components/TodoFilter';
import TodoItem from 'components/TodoItem';

class TodoList extends React.Component {
	constructor() {
		super();

		this.state = {
			name    : '',
			priority: ''
		};

		this.handleName     = this.handleName.bind(this);
		this.handlePriority = this.handlePriority.bind(this);
	}

	render() {
		let todos = this.props.todos.map((todo) =>
			<TodoItem
				remove={this.props.remove}
				toggle={this.props.toggle}
				key={todo.id}
				index={todo.id}
				name={todo.name}
				nameFilter={this.state.name}
				priority={todo.priority}
				priorityFilter={this.state.priority}
				done={todo.done}
			/>
		);

		let count     = this.props.todos.length;
		let completed = this.props.completed;

		return (
			<div className={ count > 0 ? 'section' : '' }>
				{
					count > 0
					&&
					<aside className="menu">
						<TodoProgress count={count} completed={completed} />
						<h3 className="title is-3">Todo List<span>({completed}/{count})</span>:</h3>

						<div className="notification">
							<div className="columns">
								<div className="column is-5">
									<button
										disabled={! completed}
										className="button is-warning"
										onClick={() => this.props.openModal('Delete All Done Todos ?', 'Are you sure you want to delete all completed tasks ?', this.props.removeDone)}
									>Delete Done</button>
								</div>
								<div className="column is-5">
									<button
										className="button is-warning"
										onClick={() => this.props.openModal('Delete All Todos ?', 'Are you sure you want to delete all the tasks ?', this.props.removeAll)}
									>Delete All</button>
								</div>
								<div className="column is-1">
									<button disabled={completed === count} className="button is-success is-pulled-right" onClick={() => this.props.toggleAll()}>Done All</button>
								</div>
								<div className="column is-1">
									<button disabled={! completed} className="button is-danger is-pulled-right" onClick={() => this.props.toggleAll(false)}>Undo All</button>
								</div>
							</div>

							<TodoFilter name={this.state.name} handleName={this.handleName} priority={this.state.priority} handlePriority={this.handlePriority} />
						</div>

						<ul className="menu-list">
							{todos}
						</ul>
					</aside>
				}
			</div>
		);
	}

	handleName(event) {
		this.setState({
			name: event.target.value
		});
	}

	handlePriority(event) {
		this.setState({
			priority: event.target.value
		});
	}
}

export default TodoList;