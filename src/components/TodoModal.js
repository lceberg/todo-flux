import React from 'react';

class TodoModal extends React.Component {
	render() {
		return (
			<div className={ 'modal ' + ( this.props.data.active ? 'is-active' : '' ) }>
				<div className="modal-background"></div>
				<div className="modal-content">
					<header className="modal-card-head">
						<p className="modal-card-title">{ this.props.data.title }</p>
					</header>

					<section className="modal-card-body">
						<div className="content">
							<p>{ this.props.data.content }</p>
						</div>
					</section>

					<footer className="modal-card-foot">
						<a className="button is-success" onClick={this.props.data.callback}>Yes</a>
						<a className="button is-danger" onClick={this.props.close}>No</a>
					</footer>
				</div>
				<button className="modal-close" onClick={this.props.close}></button>
			</div>
		);
	}
}

export default TodoModal;