import React from 'react';

class TodoItem extends React.Component {

	constructor() {
		super();

		this.isHidden = this.isHidden.bind(this);
		this.remove   = this.remove.bind(this);
		this.toggle   = this.toggle.bind(this);
	}

	render() {

		return (
			<li className={'columns ' + (this.isHidden() ? 'is-hidden-desktop' : '')}>
				<div className="column is-1">
					<button className="button is-warning is-small" onClick={this.remove}>Delete</button>
				</div>

				<div className="column is-4">
					<strong>{this.props.name}</strong>
				</div>

				<div className="column is-6">
					Priority: <strong>{this.props.priority}</strong>
				</div>

				<div className="column">
					<button className={ 'button is-pulled-right is-small ' + ( this.props.done ? 'is-success' : 'is-danger' ) } onClick={this.toggle}>
						{ this.props.done ? 'Undo' : 'Done' }
					</button>
				</div>

			</li>
		);
	}

	isHidden() {
		let hidden = false;

		if ( this.props.priorityFilter && this.props.priorityFilter !== this.props.priority ) {
			hidden = true;
		}

		if ( this.props.nameFilter && this.props.name.toLowerCase().indexOf(this.props.nameFilter.toLowerCase()) === -1 ) {
			hidden = true;
		}

		return hidden;
	}

	remove() {
		this.props.remove( this.props.index );
	}

	toggle() {
		this.props.toggle( this.props.index, ! this.props.done );
	}
}

export default TodoItem;