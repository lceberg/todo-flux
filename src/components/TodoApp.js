import React from 'react';

import TodoStore from 'stores/TodoStore';
import ModalStore from 'stores/ModalStore';
import * as TodoActions from 'actions/TodoActions';
import * as ModalActions from 'actions/ModalActions';
import TodoList from 'components/TodoList';
import TodoInput from 'components/TodoInput';
import TodoModal from 'components/TodoModal';

class TodoApp extends React.Component {
	constructor() {
		super();

		this.state        = {};

		this.state.todos  = TodoStore.getTodos();
		this.state.modal  = ModalStore.getModalData();

		this.getTodos     = this.getTodos.bind(this);
		this.getModalData = this.getModalData.bind(this);
		this.openModal    = this.openModal.bind(this);
		this.closeModal   = this.closeModal.bind(this);
		this.remove       = this.remove.bind(this);
		this.removeDone   = this.removeDone.bind(this);
		this.removeAll    = this.removeAll.bind(this);
		this.toggle       = this.toggle.bind(this);
		this.toggleAll    = this.toggleAll.bind(this);
	}

	componentWillMount() {
		TodoStore.on( 'change', this.getTodos );
		ModalStore.on( 'change', this.getModalData );
	}

	componentWillUnmount() {
		TodoStore.removeListener( 'change', this.getTodos );
		ModalStore.removeListener( 'change', this.getModalData );
	}

	render() {
		return (
			<div className="container">
				<div className="section">
					<h1 className="title is-1">Add Todo:</h1>
					<TodoInput />
				</div>
				<TodoList
					openModal={this.openModal}
					remove={this.remove}
					removeDone={this.removeDone}
					removeAll={this.removeAll}
					toggleAll={this.toggleAll}
					toggle={this.toggle}
					todos={this.state.todos.items}
					completed={this.state.todos.completed}
				/>
				<TodoModal data={this.state.modal} close={this.closeModal} />
			</div>
		);
	}

	getTodos() {
		this.setState({
			todos: TodoStore.getTodos()
		});
	}

	getModalData() {
		this.setState({
			modal: ModalStore.getModalData()
		});
	}

	openModal( title, content, callback ) {
		ModalActions.openModal( title, content, callback );
	}

	closeModal() {
		ModalActions.closeModal();
	}

	remove(index) {
		TodoActions.removeTodo(index);
	}

	removeDone() {
		TodoActions.removeAllDoneTodos();
		this.closeModal();
	}

	removeAll() {
		TodoActions.removeAllTodos();
		this.closeModal();
	}

	toggle( index, status ) {
		TodoActions.toggleTodo( index, status );
	}

	toggleAll( status = true ) {
		TodoActions.toggleAllTodos( status );
	}
}

export default TodoApp;